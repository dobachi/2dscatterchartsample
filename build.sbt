name := "2DScatterChartSample"

version := "1.0"

scalaVersion := "2.10.4"

resolvers ++= Seq(
  "cloudera" at "https://repository.cloudera.com/artifactory/cloudera-repos/",
  "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.0.M5b" % "test" withSources() withJavadoc(),
  "org.scalacheck" %% "scalacheck" % "1.10.0" % "test" withSources() withJavadoc(),
  "com.github.scopt" %% "scopt" % "3.2.0",
  "commons-io" % "commons-io" % "2.4",
  "com.github.wookietreiber" %% "scala-chart" % "latest.integration",
  "org.apache.hadoop" % "hadoop-client" % "2.5.0-cdh5.2.0" % "provided" withJavadoc()
)
