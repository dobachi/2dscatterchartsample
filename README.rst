###################
README
###################

This is a utility used in sample applications of Spark MLlib.

*************
Assumption
*************
* You can use sbt command.

****************
How to make JAR
****************
You can create JAR by the following command::

 $ cd
 $ mkdir Sources
 $ cd Sources
 $ git clone https://bitbucket.org/dobachi/2dscatterchartsample.git 2DScatterChartSample
 $ cd 2DScatterChartSample
 $ sbt assembly

You can get the following JAR::

 ~/Sources/2DScatterChartSample/target/scala-2.10/2DScatterChartSample.jar